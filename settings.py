import pygame
vec = pygame.math.Vector2
#definição de cores em RGB
WHITE = (255,255,255)
BLACK = (0,0,0)
DARKGRAY = (40,40,40)
LIGHTGREY = (100,100,100)
GREEN = (0,255,0)
RED = (255,0,0)
YELLOW = (255,255,0)
BROWN = (106,55,5)

#configs do jogo
WIDTH = 1366 #1366 #1024
HEIGHT = 768 #768 #768
FPS = 60
TITLE = "Nome do jogo"
BGCOLOR = BROWN

TILESIZE = 64
GRIDWIDTH = WIDTH/TILESIZE
GRIDHEIGHT = HEIGHT/TILESIZE

WALL_IMG = 'parede_verde.png'

# configurações do player
PLAYER_HEALTH = 100
PLAYER_SPEED = 250
PLAYER_ROT_SPEED = 250
PLAYER_IMG = 'personagem.png'
PLAYER_HIT_RECT = pygame.Rect(0,0,35,35)
BARREL_OFSET = vec(30, 10)

#configuração da arma
BULLET_IMG =  'bala.png'
BULLET_SPEED = 500
BULLET_LIFETIME = 1000
BULLET_RATE = 250
KICKBACK = 200
BULLET_DAMAME = 34

#configuração do inimigo
MOB_HEALTH = 100
MOB_IMG = 'mob.png'
MOB_SPEED = 150
MOB_DAMAGE = 20
MOB_KNOCKBACK = 20
MOB_HIT_RECT = pygame.Rect(0,0,30,30)
MOB_LIMIT = 10
MOB_LIMIT_SCREEN = 5
MOB_DELAY_SPANW = 10

#configuração sentri gun
SENTRI_HEALTH = 10
SENTRI_IMG = 'sentri.png'
SENTRI_DAMAGE = 5
SENTRI_HIT_RECT = pygame.Rect(0,0,30,30)


#drops
IMG_SHIELD = 'shield.png'
IMG_LIFE = 'vida.png'
LIFE_GAIN = 50





# UI
IMG_RETOMAR = 'btn_retoma.png'
