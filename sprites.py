import pygame
import random
from pygame.sprite import spritecollide
from settings import *
from tilemap import collide_hit_rect

vec = pygame.math.Vector2

def button(surf, x, y, width, height, color):
    mouse = pygame.mouse.get_pos()
    # print(mouse)
    
    draw_btn = pygame.Rect(x, y, width, height)
    pygame.draw.rect(surf, color, draw_btn )
    


def collide_with_Walls(sprite, group, dir):
    if dir == 'x':
        hits = pygame.sprite.spritecollide(sprite, group, False, collide_hit_rect)
        if hits:
            if hits[0].rect.centerx > sprite.hit_rect.centerx:
                sprite.pos.x = hits[0].rect.left - sprite.hit_rect.width / 2
            if hits[0].rect.centerx < sprite.hit_rect.centerx:
                sprite.pos.x = hits[0].rect.right + sprite.hit_rect.width / 2
            sprite.vel.x = 0
            sprite.hit_rect.centerx = sprite.pos.x

    if dir == 'y':
        hits = pygame.sprite.spritecollide(sprite, group, False, collide_hit_rect)
        if hits:
            if hits[0].rect.centery > sprite.hit_rect.centery:
                sprite.pos.y = hits[0].rect.top - sprite.hit_rect.height / 2
            if hits[0].rect.centery < sprite.hit_rect.centery:
                sprite.pos.y = hits[0].rect.bottom +  sprite.hit_rect.height / 2
            sprite.vel.y = 0
            sprite.hit_rect.centery = sprite.pos.y


class Player(pygame.sprite.Sprite):
    def __init__(self, game, x, y):
        self.groups = game.all_sprites
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.game = game
        # self.image = pygame.Surface((TILESIZE, TILESIZE))
        self.image = game.player_img
        # self.image.fill(YELLOW)
        self.rect = self.image.get_rect()
        self.hit_rect = PLAYER_HIT_RECT
        self.hit_rect.center = self.rect.center
        self.vel = vec(0,0)
        self.pos = vec(x,y)*TILESIZE
        self.vx, self.vy = 0,0
        self.x = x * TILESIZE
        self.y = y * TILESIZE
        self.rot = 0
        self.last_shot = 0
        self.health = PLAYER_HEALTH
        self.pause = False

    

    def get_keys(self):
        self.rot_speed = 0
        self.vel = vec(0,0)
        self.vx, self.vy = 0,0
        pressionado = pygame.key.get_pressed()
        if pressionado[pygame.K_a]:
            self.rot_speed = PLAYER_ROT_SPEED
            # self.player.move(dx=-1)
        if pressionado[pygame.K_d]:
            # self.player.move(dx=+1)
            self.rot_speed = -PLAYER_ROT_SPEED
        if pressionado[pygame.K_w]:
            # self.vel.y = -PLAYER_SPEED
            self.vel = vec(PLAYER_SPEED,0).rotate(-self.rot)

            # self.player.move(dy=-1)
        if pressionado[pygame.K_s]:
            self.vel = vec(-PLAYER_SPEED/2,0).rotate(-self.rot)

            # self.player.move(dy=+1)
        if pressionado[pygame.K_e]:
            now = pygame.time.get_ticks()
            if now - self.last_shot > BULLET_RATE :
                self.last_shot = now
                dir = vec(1, 0).rotate(-self.rot)
                pos = self.pos + BARREL_OFSET.rotate(-self.rot)
                Bullet(self.game, pos, dir)
                self.vel = vec(-KICKBACK, 0).rotate(-self.rot)
        
        
    

    def update(self):
        self.get_keys()
        self.rot = (self.rot+self.rot_speed*self.game.dt)%360
        self.image = pygame.transform.rotate(self.game.player_img, self.rot)
        self.rect = self.image.get_rect()
        self.rect.center = self.pos
        self.pos+= self.vel *self.game.dt
        self.hit_rect.centerx = self.pos.x
        collide_with_Walls(self, self.game.walls,'x')
        self.hit_rect.centery = self.pos.y
        collide_with_Walls(self, self.game.walls,'y')
        self.rect.center = self.hit_rect.center
        
class Sentri(pygame.sprite.Sprite):
    def __init__(self, game, x, y):
        self.groups = game.all_sprites, game.mobs
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.game = game
        self.image = game.sentri_img
        self.rect = self.image.get_rect()
        self.hit_rect = MOB_HIT_RECT.copy()
        self.hit_rect.center = self.rect.center
        self.pos = vec(x,y)*TILESIZE
        self.vel = vec(0,0)
        self.acc = vec(0,0)
        self.rect.center = self.pos
        self.rot = 180
        self.health = MOB_HEALTH
        self.last_shot = 0
        if self.health <= 0:
            self.kill

    def update(self):
        self.rot = (self.game.player.pos - self.pos).angle_to(vec(1,0))
        self.image = pygame.transform.rotate(self.game.sentri_img, self.rot)
        self.rect = self.image.get_rect()
        self.rect.center = self.pos
        self.hit_rect.centerx = self.pos.x
        collide_with_Walls(self, self.game.walls, 'x')
        self.hit_rect.centery = self.pos.y
        collide_with_Walls(self, self.game.walls, 'y')
        self.rect.center = self.hit_rect.center
        now = pygame.time.get_ticks()
        if now - self.last_shot > BULLET_RATE :
            self.last_shot = now
            dir = vec(1, 0).rotate(-self.rot)
            pos = self.pos + BARREL_OFSET.rotate(-self.rot)
            bullet_enemy(self.game, pos, dir)
            # self.vel = vec(-KICKBACK, 0).rotate(-self.rot)
        if self.health <= 0:
            if (random.randint(0,9) < 1):
                Life_drop(self.game, self.pos)
                if self.enemy_killed < MOB_LIMIT:
                    self.enemy_killed += 1
            self.kill()

    def draw_health(self):
        if self.health > 60:
            col = GREEN
        elif self.health > 30:
            col = YELLOW
        else :
            col = RED

        width = int(self.rect.width * self.health / MOB_HEALTH)
        self.health_bar = pygame.Rect(0,0, width, 7)
        if self.health< MOB_HEALTH:
            pygame.draw.rect(self.image, col, self.health_bar)

class Mob(pygame.sprite.Sprite):
    def __init__(self, game, x, y):
        self.groups = game.all_sprites, game.mobs
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.game = game
        self.image = game.mob_img
        self.rect = self.image.get_rect()
        self.hit_rect = MOB_HIT_RECT.copy()
        self.hit_rect.center = self.rect.center
        self.pos = vec(x,y)*TILESIZE
        self.vel = vec(0,0)
        self.acc = vec(0,0)
        self.rect.center = self.pos
        self.rot = 0
        self.health = MOB_HEALTH
        if self.health <= 0:
            
            if self.enemy_killed < MOB_LIMIT:
                self.enemy_killed += 1
            self.kill()

    
        
    
    def update(self):
        self.rot = (self.game.player.pos - self.pos).angle_to(vec(1,0))
        self.image = pygame.transform.rotate(self.game.mob_img, self.rot)
        self.rect = self.image.get_rect()
        self.rect.center = self.pos
        self.acc = vec(MOB_SPEED,0).rotate(-self.rot)
        self.acc +=self.vel * -1
        self.vel += self.acc * self.game.dt
        self.pos += self.vel * self.game.dt + 0.5 * self.acc * self.game.dt ** 2
        self.hit_rect.centerx = self.pos.x
        collide_with_Walls(self, self.game.walls, 'x')
        self.hit_rect.centery = self.pos.y
        collide_with_Walls(self, self.game.walls, 'y')
        self.rect.center = self.hit_rect.center
        if self.health <= 0:
            if (random.randint(0,9) < 1):
                Life_drop(self.game, self.pos)
            self.kill()

    def draw_health(self):
        if self.health > 60:
            col = GREEN
        elif self.health > 30:
            col = YELLOW
        else :
            col = RED

        width = int(self.rect.width * self.health / MOB_HEALTH)
        self.health_bar = pygame.Rect(0,0, width, 7)
        if self.health< MOB_HEALTH:
            pygame.draw.rect(self.image, col, self.health_bar)

class Shield(pygame.sprite.Sprite):
    def __init__(self, game, pos):
        self.groups = game.all_sprites, game.drop
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.game = game
        self.image = game.shield_img
        self.rect = self.image.get_rect()
        self.pos = vec(pos)
        self.rect.center = pos

class Life_drop(pygame.sprite.Sprite):
    def __init__(self, game, pos):
        self.groups = game.all_sprites, game.drop
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.game = game
        self.image = game.life_img
        self.rect = self.image.get_rect()
        self.pos = vec(pos)
        self.rect.center = pos

    
        
        

class Bullet(pygame.sprite.Sprite):
    def __init__(self, game, pos, dir):
        self.groups = game.all_sprites, game.bullets
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.game = game
        self.image = game.bullet_img
        self.rect = self.image.get_rect()
        self.pos = vec(pos)
        self.rect.center = pos
        self.vel = dir * BULLET_SPEED
        self.spawn_time = pygame.time.get_ticks()


    def update(self):
        self.pos += self.vel * self.game.dt
        self.rect.center = self.pos
        if pygame.sprite.spritecollideany(self, self.game.walls):        
            self.kill()
        if(pygame.time.get_ticks() - self.spawn_time > BULLET_LIFETIME):
            self.kill()

class bullet_enemy(pygame.sprite.Sprite):
    def __init__(self, game, pos, dir):
        self.groups = game.all_sprites, game.bullets
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.game = game
        self.image = game.bullet_img
        self.rect = self.image.get_rect()
        self.pos = vec(pos)
        self.rect.center = pos
        self.vel = dir * BULLET_SPEED
        self.spawn_time = pygame.time.get_ticks()


    def update(self):
        self.pos += self.vel * self.game.dt
        self.rect.center = self.pos
        if pygame.sprite.spritecollideany(self, self.game.walls):        
            self.kill()
        if(pygame.time.get_ticks() - self.spawn_time > BULLET_LIFETIME):
            self.kill()

class Wall(pygame.sprite.Sprite):
    def __init__(self, game, x, y):
        self.groups = game.all_sprites, game.walls
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.game = game
        self.image = game.wall_img
        # self.image.fill(GREEN)
        self.rect = self.image.get_rect()
        self.x = x
        self.y = y
        self.rect.x = x * TILESIZE
        self.rect.y = y * TILESIZE