from settings import TILESIZE
import pygame
from settings import *

def collide_hit_rect(one, two):
    return one.hit_rect.colliderect(two.rect)

def rle_decompression(text):
    new_string = ''
    num_atual = ''
    for char in text:
        if char.isdigit():
            num_atual += char
        else:
            new_string += int(num_atual or '1') * char
            num_atual = ''
    return new_string


class Map:
    def __init__(self, filename):
        self.data = []
        with open(filename, 'rt') as f:
            for line in f:
                self.data.append(rle_decompression(line.strip()))
                
                print(rle_decompression(line))
            self.tilewidth = len(self.data[0])
            self.tileheight = len(self.data)
            self.width = self.tilewidth*TILESIZE
            self.height = self.tileheight*TILESIZE

class Camera:
    def __init__(self, width, height):
        self.camera = pygame.Rect(0,0,width, height)
        self.width = width
        self.height = height

    def apply(self, entity):
        return entity.rect.move(self.camera.topleft)

    def update(self, target):
        x = -target.rect.centerx + int(WIDTH/2)
        y = -target.rect.centery + int(HEIGHT/2)


        # limite de rolagem da camera
        x = min(0,x)
        y = min(0,y)
        x = max(-(self.width - WIDTH), x)
        y = max(-(self.height - HEIGHT), y)
        self.camera = pygame.Rect(x, y, self.width, self.height)