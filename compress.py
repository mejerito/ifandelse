com = []
dec = []

def abrirArquivo(file):
    arquivo = open(file, "r")
    linhas = arquivo.readlines()
    arquivo.close()
    return linhas

def criaArquivo(texto, nome):
    arquivo = open(nome, 'w')
    arquivo.writelines(texto)

    print("linha criada com sucesso")
    arquivo.close()

def rleCompression (Text):
    res=''
    a=''
    count = 0
    for i in Text:
        count+=1
        if a.count(i)>0:
            a+=i
        else:
            if len(a)>=4:
                if len(a)<10:
                    res+=str(len(a))+a[0][:1]
                else:
                    res+= str(len(a)) + a[0][:1]
                a=i
            else:
                res+=a
                a=i
        if count == len(Text):
            if len(a)>=4:
                if len(a)<10:
                    res+=str(len(a))+a[0][:1]
                else:
                    res+= str(len(a)) + a[0][:1]
            else:
                res+=a
    return(res)
def rle_decompression(text):
    new_string = ''
    num_atual = ''
    for char in text:
        if char.isdigit():
            num_atual += char
        else:
            new_string += int(num_atual or '1') * char
            num_atual = ''
    return new_string


def code_map(text):
    for i in range(0, len(text)):
        en = rleCompression(text[i])
        de = rle_decompression(en)
        com.append(en)
        dec.append(de)
    criaArquivo(com,"compressed.txt")
    criaArquivo(dec,"decompressed.txt")

code_map(abrirArquivo('map2.txt'))