import pygame
import pgzero
import sys
import random
from os import path
from settings import *
from sprites import *
from tilemap import *

class Menu:
    # Main Menu
    def main_menu():
    
        menu=True
        selected="start"
    
        while menu:
            for event in pygame.event.get():
                if event.type==pygame.QUIT:
                    pygame.quit()
                    quit()
                if event.type==pygame.KEYDOWN:
                    if event.key==pygame.K_UP:
                        selected="start"
                    elif event.key==pygame.K_DOWN:
                        selected="quit"
                    if event.key==pygame.K_RETURN:
                        if selected=="start":
                            print("Start")
                        if selected=="quit":
                            pygame.quit()
                            quit()
    
            # Main Menu UI
            screen.fill(blue)
            title=text_format("Sourcecodester", font, 90, yellow)
            if selected=="start":
                text_start=text_format("START", font, 75, white)
            else:
                text_start = text_format("START", font, 75, black)
            if selected=="quit":
                text_quit=text_format("QUIT", font, 75, white)
            else:
                text_quit = text_format("QUIT", font, 75, black)
    
            title_rect=title.get_rect()
            start_rect=text_start.get_rect()
            quit_rect=text_quit.get_rect()
    
            # Main Menu Text
            screen.blit(title, (screen_width/2 - (title_rect[2]/2), 80))
            screen.blit(text_start, (screen_width/2 - (start_rect[2]/2), 300))
            screen.blit(text_quit, (screen_width/2 - (quit_rect[2]/2), 360))
            pygame.display.update()
            clock.tick(FPS)
            pygame.display.set_caption("Python - Pygame Simple Main Menu Selection")


# funções do HUD
def draw_player_health(surf, x, y, pct):
    if pct < 0:
        pct = 0
    BAR_LENGTH = 100
    BAR_HEIGHT = 20
    fill = pct *BAR_LENGTH
    outline_rect = pygame.Rect(x, y, BAR_LENGTH, BAR_HEIGHT)
    fill_rect = pygame.Rect(x, y, fill, BAR_HEIGHT)
    if pct > 0.6:
        col = GREEN
    elif pct > 0.3:
        col = YELLOW
    else:
        col = RED
    pygame.draw.rect(surf, col, fill_rect)
    pygame.draw.rect(surf, WHITE, outline_rect, 2)




class Game:
    def __init__(self):
        pygame.init()
        self.screen = pygame.display.set_mode((WIDTH, HEIGHT))
        pygame.display.set_caption(TITLE)
        self.clock = pygame.time.Clock()
        pygame.key.set_repeat(500,100)
        self.load_data()
        self.level = 0
        self.enemy_killed = 0


    def draw_text(self, text, font_name, size, color, x, y, align="nw"):
        font = pygame.font.Font(font_name, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        if align == "nw":
            text_rect.topleft = (x, y)
        if align == "ne":
            text_rect.topright = (x, y)
        if align == "sw":
            text_rect.bottomleft = (x, y)
        if align == "se":
            text_rect.bottomright = (x, y)
        if align == "n":
            text_rect.midtop = (x, y)
        if align == "s":
            text_rect.midbottom = (x, y)
        if align == "e":
            text_rect.midright = (x, y)
        if align == "w":
            text_rect.midleft = (x, y)
        if align == "center":
            text_rect.center = (x, y)
        self.screen.blit(text_surface, text_rect)    

    def change_map(self, fase = 0):
        selecao_mapa = ['mapa1.txt', 'mapa2.txt']
        
        game_folder = path.dirname(__file__)
        self.map = Map(path.join(game_folder, selecao_mapa[fase]))
        


    def load_data(self):
        game_folder = path.dirname(__file__)
        img_folder = path.join(game_folder, 'img')
        self.font = path.join(img_folder, 'CutiveMono-Regular.ttf')
        self.dim_screen = pygame.Surface(self.screen.get_size()).convert_alpha()
        self.dim_screen.fill((0, 0, 0, 180))     
        # self.change_map(self.level)
        self.change_map()
        self.player_img = pygame.image.load(path.join(img_folder, PLAYER_IMG)).convert_alpha()
        self.btn_retomar_img = pygame.image.load(path.join(img_folder, IMG_RETOMAR)).convert_alpha()
        self.bullet_img = pygame.image.load(path.join(img_folder, BULLET_IMG)).convert_alpha()
        self.shield_img = pygame.image.load(path.join(img_folder, IMG_LIFE)).convert_alpha()
        self.life_img = pygame.image.load(path.join(img_folder, IMG_LIFE)).convert_alpha()
        self.mob_img = pygame.image.load(path.join(img_folder, MOB_IMG)).convert_alpha()
        self.sentri_img = pygame.image.load(path.join(img_folder, SENTRI_IMG)).convert_alpha()
        self.wall_img = pygame.image.load(path.join(img_folder, WALL_IMG)).convert_alpha()
        self.wall_img = pygame.transform.scale(self.wall_img, (TILESIZE, TILESIZE))
        self.main_music = pygame.mixer.Sound('Map.wav')

        


    def new(self):
        # inicializa todas as variaveis e toda a configuração para um new game
        self.all_sprites = pygame.sprite.Group()
        self.walls = pygame.sprite.Group()
        self.mobs = pygame.sprite.Group()
        self.bullets = pygame.sprite.Group()
        self.drop = pygame.sprite.Group()
        self.pos_spaw = []
        self.level = 0
        self.main_music.set_volume(0.25)
        self.main_music.play(-1)

        
        for row, tiles in enumerate(self.map.data):
            for col, tile in enumerate(tiles):
                if tile == 'W':
                    Wall(self, col, row)
                if tile == 'M':
                    Mob(self, col, row)
                    self.pos_spaw.append(vec(col, row))
                if tile == 'S':
                    Sentri(self, col, row)
                if tile == 'P':
                     self.player = Player(self, col,row)
                
        self.paused = False          
        self.camera = Camera(self.map.width, self.map.height)


    def run(self):
        # loop de jogo
        self.playing = True
        while self.playing:
            self.dt = self.clock.tick(FPS)/1000
            self.events()
            if not self.paused:
                self.update()
            self.draw()
    def quit(self):
        pygame.quit()
        sys.exit()

    def update(self):
        self.all_sprites.update()
        self.camera.update(self.player)
        self.lastEnemy = 0
        
        # mob acertando o player
        hits = pygame.sprite.spritecollide(self.player, self.mobs, False, collide_hit_rect)
        dropHits = pygame.sprite.spritecollide(self.player, self.drop, False)
        for hit in hits:
            self.player.health -= MOB_DAMAGE
            hit.vel = vec(0,0)
            if self.player.health <= 0:
                self.playing = False
                self.main_music.stop()
        if hits:
            self.player.pos += vec(MOB_KNOCKBACK, 0).rotate(-hits[0].rot)
        #balas acertando os mobs
        hits = pygame.sprite.groupcollide(self.mobs, self.bullets, False, True)
        for hit in hits:
            hit.health -= BULLET_DAMAME
            hit.vel = vec(0,0)

        for drophit in dropHits:
            
            if self.player.health < PLAYER_HEALTH:
                if (self.player.health + LIFE_GAIN) >= PLAYER_HEALTH:
                    self.player.health = PLAYER_HEALTH
                else:
                    self.player.health += LIFE_GAIN
            drophit.kill()
        now = pygame.time.get_ticks()
        if now - self.lastEnemy > MOB_DELAY_SPANW:
            if len(self.mobs) < MOB_LIMIT_SCREEN:
                
                Mob(self, self.pos_spaw[random.randint(0, len(self.pos_spaw)-1)].x,self.pos_spaw[random.randint(0, len(self.pos_spaw)-1)].y)
        
    
    def draw_grid(self):
        for x in range(0, WIDTH, TILESIZE):
            pygame.draw.line(self.screen, LIGHTGREY, (x, 0), (x, HEIGHT))
        for y in range(0, HEIGHT, TILESIZE):
            pygame.draw.line(self.screen, LIGHTGREY, (0,y), (WIDTH, y))
    def draw(self):
        pygame.display.set_caption("{:.2f}".format(self.clock.get_fps()))
        self.screen.fill(BGCOLOR)
        # self.draw_grid()
        # self.all_sprites.draw(self.screen)
        for sprite in self.all_sprites:
            if isinstance(sprite, Mob):
                sprite.draw_health()
            self.screen.blit(sprite.image, self.camera.apply(sprite))
        # pygame.draw.rect(self.screen, WHITE, self.camera.apply(self.player),2)
        # teste de colisão
        # pygame.draw.rect(self.screen, WHITE, self.player.hit_rect,2)

        # funções do HUD
        draw_player_health(self.screen, 10, 10, self.player.health/PLAYER_HEALTH)
        if self.paused:
            self.screen.blit(self.dim_screen, (0,0))  
            self.draw_text("Pausa", self.font, 195, WHITE, WIDTH/2, HEIGHT/2, align="center")
        pygame.display.flip()


    def events(self):
        # captura todos os eventos
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.paused = not self.paused
            
    def show_start_screen(self):
        pass


    def show_go_screen(self):
        pass
    # cria os gameobjects
g = Game()
g.show_start_screen()
while True:
    g.new()
    g.run()
    g.show_go_screen()
